﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;

using UnityEngine;

public class NetEngine
{
    BroadcastNet net;

    PacketPlayerState localPlayerState;

    //List<PacketPlayerState> externalPlayersStates;
    public ConcurrentDictionary<Guid, PacketPlayerState> ExternalPlayersStates;

    public NetEngine()
    {
        this.net = new BroadcastNet(25565);// For fun... minecraft port :-)

        this.localPlayerState = new PacketPlayerState();
        this.localPlayerState.ID = Guid.NewGuid();

        this.ExternalPlayersStates = new ConcurrentDictionary<Guid, PacketPlayerState>();

        Thread thread = new Thread(new ThreadStart(ProcessQueue));
        thread.Start();
    }

    public void ProcessQueue()
    {
        while(true)
        {
            Byte[] toProces;

            while (net.PacketsQueue.TryDequeue(out toProces))
            {
                // TODO: Pridat kontrolu vice packetu najednou

                // Kontrola, ze je packet pro nas
                if(toProces[0] == localPlayerState.PacketID0 && toProces[1] == localPlayerState.PacketID1 && toProces[2] == localPlayerState.PacketID2)
                {
                    PacketPlayerState externalPlayer = new PacketPlayerState();
                    long readPos = externalPlayer.SetSerialized(toProces);

                    if (readPos != toProces.Length)
                        System.Console.WriteLine("UDP packet is longer");

                    // Casova znamka
                    externalPlayer.ReceivingTime = DateTime.Now;

                    // Zajimaji nas jen ostatni hraci
                    if (externalPlayer.ID != this.localPlayerState.ID)
                    {
                        this.ExternalPlayersStates[externalPlayer.ID] = externalPlayer;
                    }

                }
            }
            Thread.Sleep(5);
        }
    }

    public void SetLocalPlayerState(
        string name,
        float bestTime,
        float red, float green, float blue,
        float Px, float Py, float Pz,
        float GPx, float GPy, float GPz,
        bool acceleration,
        int globeIndex,
        string tweet)
    {
        this.localPlayerState.Name = name;
        this.localPlayerState.BestTime = bestTime;

        this.localPlayerState.Red = red;
        this.localPlayerState.Green = green;
        this.localPlayerState.Blue = blue;

        this.localPlayerState.PositionX = Px;
        this.localPlayerState.PositionY = Py;
        this.localPlayerState.PositionZ = Pz;

        this.localPlayerState.GlobePositionX = GPx;
        this.localPlayerState.GlobePositionY = GPy;
        this.localPlayerState.GlobePositionZ = GPz;

        //this.localPlayerState.PositionVelocityX = PVx;
        //this.localPlayerState.PositionVelocityY = PVy;
        //this.localPlayerState.PositionVelocityZ = PVz;

        //this.localPlayerState.PositionVelocityX = PAx;
        //this.localPlayerState.PositionVelocityY = PAy;
        //this.localPlayerState.PositionVelocityZ = PAz;
        this.localPlayerState.Acceleration = acceleration ? 1 : 0;
        this.localPlayerState.GlobeIndex = globeIndex;
        this.localPlayerState.Tweet = tweet;

        net.SendPacket(this.localPlayerState.GetInBytes());
    }

    //public void SetLocalPlayerState(
    //    string name,
    //    float bestTime,
    //    float red, float green, float blue,
    //    float Px, float Py, float Pz, 
    //    float PVx, float PVy, float PVz, 
    //    float PAx, float PAy, float PAz,
    //    bool acceleration)
    //{
    //    this.localPlayerState.Name = name;
    //    this.localPlayerState.BestTime = bestTime;

    //    this.localPlayerState.Red = red;
    //    this.localPlayerState.Green = green;
    //    this.localPlayerState.Blue = blue;

    //    this.localPlayerState.PositionX = Px;
    //    this.localPlayerState.PositionY = Py;
    //    this.localPlayerState.PositionZ = Pz;

    //    this.localPlayerState.PositionVelocityX = PVx;
    //    this.localPlayerState.PositionVelocityY = PVy;
    //    this.localPlayerState.PositionVelocityZ = PVz;

    //    this.localPlayerState.PositionVelocityX = PAx;
    //    this.localPlayerState.PositionVelocityY = PAy;
    //    this.localPlayerState.PositionVelocityZ = PAz;
    //    this.localPlayerState.Acceleration = acceleration ? 1 : 0;

    //    net.SendPacket(this.localPlayerState.GetInBytes());
    //}

    public PacketPlayerState GetLocalPlayerStat()
    {
        return this.localPlayerState;
    }
}
