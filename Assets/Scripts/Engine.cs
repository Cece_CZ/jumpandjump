﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Audio;
using TMPro;
using System.Collections.Concurrent;

/// <summary>
/// Hlavni logika hry
/// </summary>
public class Engine : MonoBehaviour
{
    /// <summary>
    /// Lokalni hrac
    /// </summary>
    GameObject localPlayer;

    /// <summary>
    /// Odkaz na program lokalniho hrace
    /// </summary>
    public Player LocalPlayerScript;

    /// <summary>
    /// Vsichni ostatni hraci
    /// </summary>
    Dictionary<Guid, GameObject> externalPlayers;

    /// <summary>
    /// Zpracovava stav hry
    /// </summary>
    NetEngine netEngine;

    /// <summary>
    /// Mixer pro oddelene nastavovani hlasitosti
    /// </summary>
    public AudioMixer audioMixer;

    /// <summary>
    /// Seznam vsech globu ke sbirani
    /// </summary>
    List<GameObject> globes;

    /// <summary>
    /// Aktualne aktivni glob
    /// </summary>
    GameObject currentGlobe;

    /// <summary>
    /// 2D overlay pri hre
    /// </summary>
    GameObject game;

    /// <summary>
    /// 2D overlay v menu
    /// </summary>
    GameObject menu;

    /// <summary>
    /// 2D overlay vysledku
    /// </summary>
    GameObject scoreBoard;

    /// <summary>
    /// Skript vysledku
    /// </summary>
    ScoreBoard scoreBoardScript;

    /// <summary>
    /// 2D overlay chatu
    /// </summary>
    Chat chat;

    /// <summary>
    /// Text s aktualnim casem
    /// </summary>
    TextMeshProUGUI textTime;

    /// <summary>
    /// Text s perzistentne nejlepsim casem
    /// </summary>
    TextMeshProUGUI textBestTimeEver;

    /// <summary>
    /// Text s nejlepsim casem od spusteni
    /// </summary>
    TextMeshProUGUI textBestTime;

    /// <summary>
    /// Text s poctem sebranych globu
    /// </summary>
    TextMeshProUGUI textProgress;

    /// <summary>
    /// Priznak zda se meri cas (do sebrani prvniho globu nebezi)
    /// </summary>
    public bool IsTimeRunning;

    /// <summary>
    /// Vzdalenost ve ktere se sebere glob
    /// </summary>
    public float GlobeCollisionDistance = 1;

    /// <summary>
    /// Vzdalenost, kdy dojde ke kolizi mezi hraci
    /// </summary>
    public float PlayersMinDistance = 1.0f;

    /// <summary>
    /// Za jak dlouho se smaze jiny hrac, prokud ze site od nej nic nepride
    /// </summary>
    public float MaxInactiveTime = 1.5f;

    /// <summary>
    /// Cas sebrani prvniho globu, od kteho se pocita cas
    /// </summary>
    float startingTime;

    /// <summary>
    /// Data ukladajici se do souboru (jmeno, nastaveni, nejlepsi cas ap.)
    /// </summary>
    PersistentData settings;

    /// <summary>
    /// Nejlepsi cas od spusteni hry
    /// </summary>
    float currentBestTime;

    /// <summary>
    /// Poradi aktivniho globu
    /// </summary>
    int globeIndex;

    /// <summary>
    /// Naposledy odeslana zprava
    /// </summary>
    string tweet;

    /// <summary>
    /// Unity Start
    /// </summary>
    void Start ()
    {
        //Application.targetFrameRate = 120;

        this.currentBestTime = float.PositiveInfinity;

        this.settings = PersistentData.LoadOrDefault();

        this.IsTimeRunning = false;

        this.localPlayer = GameObject.FindGameObjectWithTag("Player");
        this.localPlayer.transform.Find("Sphere").GetComponent<MeshRenderer>().material.color = new Color(this.settings.Red, this.settings.Green, this.settings.Blue);

        this.LocalPlayerScript = this.localPlayer.GetComponent<Player>();

        this.externalPlayers = new Dictionary<Guid, GameObject>();

        this.netEngine = new NetEngine();

        this.textTime = Camera.main.transform.Find("Canvas/Game/Time").gameObject.GetComponent<TextMeshProUGUI>();

        this.textBestTimeEver = Camera.main.transform.Find("Canvas/Game/BestTimeEver").gameObject.GetComponent<TextMeshProUGUI>();
        this.textBestTimeEver.text = "Best time ever: " + GetTimeString(this.settings.BestTime);

        this.textBestTime = Camera.main.transform.Find("Canvas/Game/BestTime").gameObject.GetComponent<TextMeshProUGUI>();
        this.textBestTime.text = "Best: --:--,-";

        this.textProgress = Camera.main.transform.Find("Canvas/Game/Progress/CollectedGlobes").gameObject.GetComponent<TextMeshProUGUI>();

        this.game = Camera.main.transform.Find("Canvas/Game").gameObject;

        this.menu = Camera.main.transform.Find("Canvas/Menu").gameObject;
        this.menu.GetComponent<Menu>().SetNameOnStart(this.settings.Name);
        this.menu.GetComponent<Menu>().SetColorOnStart(this.settings.Red, this.settings.Green, this.settings.Blue);
        this.menu.GetComponent<Menu>().SetSound(this.settings.Sound);
        this.menu.GetComponent<Menu>().SetMusic(this.settings.Music);
        this.menu.GetComponent<Menu>().SetMouseSensitivity(this.settings.MouseSensitivity);

        this.scoreBoard = Camera.main.transform.Find("Canvas/ScoreBoard").gameObject;
        this.scoreBoardScript = this.scoreBoard.GetComponent<ScoreBoard>();

        this.chat = Camera.main.transform.Find("Canvas/Chat").gameObject.GetComponent<Chat>();

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        this.SetSound(this.settings.Sound);
        this.SetMusic(this.settings.Music);
        this.SetSensitivity(this.settings.MouseSensitivity);

        GameObject pickupGlobes = GameObject.Find("World/Level/PickupGlobes");
        int count = pickupGlobes.transform.childCount;
        this.globes = new List<GameObject>();
        for(int i = 0; i < count; ++i)
        {
            this.globes.Add(pickupGlobes.transform.GetChild(i).gameObject);
        }

        this.ActiveFirst();

        this.tweet = "";
    }
	
    /// <summary>
    /// Unity Update
    /// </summary>
	void Update ()
    {
        // *************************KEYS*******************************************
        if (Input.GetButtonUp("Cancel")) // Do Menu
        {
            if (this.chat.Active)
            {
                this.LocalPlayerScript.IsControled = true;
                this.chat.Hide();
            }
            else
            { 
                bool newState = !this.menu.activeSelf;
                this.LocalPlayerScript.IsControled = !newState;
                Cursor.visible = newState;
                Cursor.lockState = newState ? CursorLockMode.None : CursorLockMode.Locked;

                Debug.Log("Menu " + newState);
                this.game.SetActive(!newState);
                this.menu.SetActive(newState);
            }
        }

        if (Input.GetKey(KeyCode.Tab)) // Ukazani vysledku
        {
            if (this.LocalPlayerScript.IsControled && !this.scoreBoard.activeSelf)
                this.scoreBoard.SetActive(true);
        }
        else
        {
            if (this.scoreBoard.activeSelf)
                this.scoreBoard.SetActive(false);
        }

        if(Input.GetKeyUp(KeyCode.Return))
        {
            if (this.chat.Active)
            {
                this.LocalPlayerScript.IsControled = true;
                this.chat.Hide();
            }
            else
            {
                if (this.LocalPlayerScript.IsControled)
                {
                    this.LocalPlayerScript.IsControled = false;
                    this.chat.Show(new Color(this.netEngine.GetLocalPlayerStat().Red, this.netEngine.GetLocalPlayerStat().Green, this.netEngine.GetLocalPlayerStat().Blue));
                }
            }         
        }

        if(this.LocalPlayerScript.IsControled)
        {
            if (Input.GetKeyUp(KeyCode.R)) // Reset levelu
            {
                this.ActiveFirst();
            }
        }

        // **********************FUNKCE**********************************************************

        if (this.IsTimeRunning)
            this.textTime.text = GetTimeString(Time.time - this.startingTime);

        List<Guid> toDelete = new List<Guid>();

        foreach (var pps in this.netEngine.ExternalPlayersStates)
        {
            if (this.externalPlayers.ContainsKey(pps.Value.ID))
            {
                float dTime = (float)(DateTime.Now - pps.Value.ReceivingTime).TotalSeconds;
                Vector3 basePosition = new Vector3(pps.Value.PositionX, pps.Value.PositionY, pps.Value.PositionZ);

                this.externalPlayers[pps.Value.ID].transform.position = basePosition;

                Vector3 ghostPosition = new Vector3(pps.Value.GlobePositionX, pps.Value.GlobePositionY, pps.Value.GlobePositionZ);
                this.externalPlayers[pps.Value.ID].transform.GetChild(0).position = ghostPosition;
                LineRenderer lr = this.externalPlayers[pps.Value.ID].GetComponent<LineRenderer>();
                lr.SetPosition(0, basePosition);
                lr.SetPosition(1, ghostPosition);

                // Zmena stavu TODO: vylepsit to hledani
                this.externalPlayers[pps.Value.ID].GetComponent<PlayerExternal>().SetName(pps.Value.Name);
                this.externalPlayers[pps.Value.ID].GetComponent<PlayerExternal>().SetColor(pps.Value.Red, pps.Value.Green, pps.Value.Blue);

                //string t = (pps.Value.Name + " " + pps.Value.Tweet)
                this.chat.UpdateTweet(pps.Value.ID, new Color(pps.Value.Red, pps.Value.Green, pps.Value.Blue, 0.5f), pps.Value.Name, pps.Value.Tweet);
            }
            else // Musime vytvorit noveho externiho hrace
            {
                UnityEngine.Object prefab = Resources.Load("PlayerExternal");
                GameObject instance = Instantiate(prefab) as GameObject;
                instance.transform.position = new Vector3(pps.Value.PositionX, pps.Value.PositionY, pps.Value.PositionZ);
                this.externalPlayers.Add(pps.Value.ID, instance);
            }

            // Kontrola dlouhe necinnosti
            if((DateTime.Now - pps.Value.ReceivingTime).TotalSeconds > this.MaxInactiveTime)
            {
                //Odstranime
                Destroy(this.externalPlayers[pps.Value.ID]);
                this.externalPlayers.Remove(pps.Value.ID);
                this.chat.RemovePlayer(pps.Value.ID);

                toDelete.Add(pps.Value.ID);
            }
        }

        foreach(Guid id in toDelete)
        {
            PacketPlayerState pps = new PacketPlayerState();
            this.netEngine.ExternalPlayersStates.TryRemove(id, out pps);
        }

        this.CheckForAccEffect();
        this.CheckForOthersCollisions();
        this.CheckForPlayersCollisons();
        this.CheckForGlobeCollision();
    }

    /// <summary>
    /// Unity FixedUpdate
    /// </summary>
    private void FixedUpdate()
    {
        this.netEngine.SetLocalPlayerState(
            this.settings.Name,
            this.currentBestTime,
            this.settings.Red, this.settings.Green, this.settings.Blue,
            this.localPlayer.transform.position.x,
            this.localPlayer.transform.position.y,
            this.localPlayer.transform.position.z,
            this.currentGlobe.transform.position.x,
            this.currentGlobe.transform.position.y,
            this.currentGlobe.transform.position.z,
            this.LocalPlayerScript.IsAccelerationOn(),
            this.globeIndex,
            this.tweet
            );

        this.scoreBoardScript.UpdateData(this.netEngine.GetLocalPlayerStat(), this.netEngine.ExternalPlayersStates, globes.Count);

        this.chat.UpdateTweet(
            this.netEngine.GetLocalPlayerStat().ID,
            new Color(this.netEngine.GetLocalPlayerStat().Red, this.netEngine.GetLocalPlayerStat().Green, this.netEngine.GetLocalPlayerStat().Blue, 0.5f), 
            this.netEngine.GetLocalPlayerStat().Name, 
            this.netEngine.GetLocalPlayerStat().Tweet);
    }




    /// <summary>
    /// Zda nesebral hrac globus
    /// </summary>
    private void CheckForGlobeCollision()
    {
        if((this.localPlayer.transform.position - this.currentGlobe.transform.position).magnitude < this.GlobeCollisionDistance)
        {
            this.ActiveNextGlobe();
        }
    }

    /// <summary>
    /// Pokud se lokalni hrac priblizi na urcitou vzdalenost od jineho hrace, odmrsti ho to v protilehlem smeru
    /// </summary>
    private void CheckForPlayersCollisons()
    {
        foreach (var pps in this.externalPlayers)
        {
            Vector3 playersDistance = this.localPlayer.transform.position - pps.Value.transform.position;
            if (playersDistance.magnitude < this.PlayersMinDistance)
            {
                // Odhozeni
                this.LocalPlayerScript.RBody.AddForce(playersDistance * 8, ForceMode.Impulse);

                // Efekt
                this.LocalPlayerScript.PlayCollisionEffect(pps.Value.transform.position);              
            }
        }
    }

    /// <summary>
    /// Kontrola kolizi vsech se vsema, aby se pustil efekt
    /// </summary>
    private void CheckForOthersCollisions()
    {
        HashSet<Guid> already = new HashSet<Guid>();
        foreach (var pps1 in this.externalPlayers)
        {
            foreach (var pps2 in this.externalPlayers)
            {
                if (!already.Contains(pps1.Key))                    
                {
                    if (pps1.Key != pps2.Key)
                    {
                        if ((pps1.Value.transform.position - pps2.Value.transform.position).magnitude < this.PlayersMinDistance)
                        {
                            already.Add(pps2.Key);
                            pps2.Value.GetComponent<PlayerExternal>().PlayCollisionEffect(pps1.Value.transform.position);
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Zobrazovani efektu akcelerace u ostatnich hracu
    /// </summary>
    private void CheckForAccEffect()
    {
        foreach (var pps in this.netEngine.ExternalPlayersStates)
        {
            if (pps.Value.Acceleration == 1)
            {
                this.externalPlayers[pps.Value.ID].GetComponent<PlayerExternal>().StartEffect();
            }
            else
            {
                this.externalPlayers[pps.Value.ID].GetComponent<PlayerExternal>().StopEffect();
            }          
        }
    }

    /// <summary>
    /// Zapne prvni glob v poradi
    /// </summary>
    private void ActiveFirst()
    {
        this.IsTimeRunning = false;
        this.textTime.text = GetTimeString(0);

        int count = this.globes.Count;
        for(int i = 1; i < count; ++i)
        {
            this.DisableGlobe(globes[i]);
        }
        this.currentGlobe = this.globes[0];
        this.EnableGlobe(this.currentGlobe);

        // Nastaveni textu
        this.textProgress.text = "0/" + count;
        this.globeIndex = 0;
    }

    /// <summary>
    /// Prepne na dalsi glob v poradi
    /// </summary>
    private void ActiveNextGlobe()
    {
        int nextIndex;
        int count = this.globes.Count;
        for (int i = 0; i < count; ++i)
        {
            if(this.globes[i] == this.currentGlobe)
            {
                // Kontrola, ktery globe to byl
                if (i == 0) // Start
                {
                    this.IsTimeRunning = true;
                    this.startingTime = Time.time;
                }

                if(i == count - 1) // V cili a
                {
                    this.IsTimeRunning = false;
                    // Záznam a zobrazení nejlepšího času celkove
                    if(Time.time - this.startingTime < this.settings.BestTime)
                    {
                        // Nastveni nejlepsiho casu
                        this.settings.BestTime = Time.time - this.startingTime;
                        // Zobrazeni
                        this.textBestTimeEver.text = "Best time ever:  " + GetTimeString(this.settings.BestTime);
                        // Ulozeni do souboru
                        PersistentData.Save(this.settings);
                    }

                    // Nastaveni nejlepsiho casu od zapnuti hry
                    if (Time.time - this.startingTime < this.currentBestTime)
                    {
                        this.currentBestTime = Time.time - this.startingTime;
                        this.textBestTime.text = "Best: " + GetTimeString(this.currentBestTime);
                    }

                    this.currentGlobe.transform.Find("Sounds/FinalPickup").gameObject.GetComponent<AudioSource>().Play();
                    this.currentGlobe.transform.Find("FinalParticles").gameObject.GetComponent<ParticleSystem>().Play();

                    this.ActiveFirst();
                }
                else // Skok na dalsi
                {
                    this.currentGlobe.transform.Find("TakenParticles").gameObject.GetComponent<ParticleSystem>().Play();

                    this.currentGlobe.transform.Find("Sounds/GlobePickup").gameObject.GetComponent<AudioSource>().Play();
                    this.DisableGlobe(this.currentGlobe);


                    nextIndex = ++i % count;
                    this.currentGlobe = this.globes[nextIndex];
                    this.EnableGlobe(this.currentGlobe);

                    // Nastaveni textu
                    this.textProgress.text = i + "/" + count;
                    this.globeIndex = i;
                }

                return;
            }
        }
    }

    /// <summary>
    /// Zapne glob a provede prislusne veci
    /// </summary>
    /// <param name="go"></param>
    private void EnableGlobe(GameObject go)
    {
        go.transform.Find("Visual").gameObject.SetActive(true);
        go.transform.Find("Particles").gameObject.GetComponent<ParticleSystem>().Play();
    }

    /// <summary>
    /// Vypne glob a provede prislusne veci
    /// </summary>
    /// <param name="go"></param>
    private void DisableGlobe(GameObject go)
    {
        go.transform.Find("Visual").gameObject.SetActive(false);
        go.transform.Find("Particles").gameObject.GetComponent<ParticleSystem>().Stop();
    }

    /// <summary>
    /// Prevede cas na hezky format
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    public static string GetTimeString(float time)
    {
        if (time == float.PositiveInfinity)
            return "--:--,-";

        return Mathf.FloorToInt(time / 60) + ":" + (time - Mathf.FloorToInt(time / 60) * 60).ToString("00.0");
    }

    /// <summary>
    /// Nastavi jmeno hrace a ulozi
    /// </summary>
    /// <param name="newName"></param>
    public void SetPlayersName(string newName)
    {
        this.settings.Name = newName;
        PersistentData.Save(this.settings);
    }

    /// <summary>
    /// Nastavi barvu hrace a ulozi
    /// </summary>
    /// <param name="r"></param>
    /// <param name="g"></param>
    /// <param name="b"></param>
    public void SetPlayerColor(float r, float g, float b)
    {
        this.localPlayer.transform.Find("Sphere").GetComponent<MeshRenderer>().material.color = new Color(r, g, b);
        this.settings.Red = r;
        this.settings.Green = g;
        this.settings.Blue = b;
        PersistentData.Save(this.settings);
    }

    /// <summary>
    /// Nastavi hlasitost zvuku a ulozi
    /// </summary>
    /// <param name="value"></param>
    public void SetSound(float value)
    {
        this.settings.Sound = value;
        this.audioMixer.SetFloat("Sounds", value);
        PersistentData.Save(this.settings);
    }

    /// <summary>
    /// Nastavi hlasitost hudby a ulozi
    /// </summary>
    /// <param name="value"></param>
    public void SetMusic(float value)
    {
        this.settings.Music = value;
        this.audioMixer.SetFloat("Music", value);
        PersistentData.Save(this.settings);
    }

    /// <summary>
    /// Nastavi citlivost mysi
    /// </summary>
    /// <param name="value"></param>
    public void SetSensitivity(float value)
    {
        this.settings.MouseSensitivity = value;
        this.LocalPlayerScript.SetMouseSensitivity(value);
        PersistentData.Save(this.settings);
    }

    /// <summary>
    /// Vrati aktualni glob
    /// </summary>
    /// <returns></returns>
    public GameObject GetCurrentGlobe()
    {
        return this.currentGlobe;
    }

    /// <summary>
    /// Nastavi zpravu
    /// </summary>
    /// <param name="tweet"></param>
    public void SetTweet(string tweet)
    {
        this.tweet = tweet;
    }
}
