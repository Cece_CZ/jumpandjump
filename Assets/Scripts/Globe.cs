﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Objekt ktery se ve hre na cas sbira
/// </summary>
public class Globe : MonoBehaviour
{
    /// <summary>
    /// Pocatecni pozice kolem ktere se otaci
    /// </summary>
    Vector3 startingPos;

    /// <summary>
    /// Pro nahodny start
    /// </summary>
    float seed;

    /// <summary>
    /// Vertikalni rozptyl pri otaceni
    /// </summary>
    public float amplitudeY = 0.1f;

    /// <summary>
    /// Rychlost otaceni
    /// </summary>
    public float speed = 2;

    /// <summary>
    /// Horizontalni radius otaceni
    /// </summary>
    public float horizontalRadius = 0.5f;

	// Use this for initialization
	void Start ()
    {
		this.startingPos = this.transform.position;
        this.seed = Random.Range(0, Mathf.PI);
    }
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 tempPos = this.transform.position;
        tempPos.x = this.startingPos.x + Mathf.Cos(Time.time * speed + this.seed) * horizontalRadius;
        tempPos.y = this.startingPos.y + Mathf.Sin(Time.time * speed + this.seed) * amplitudeY;
        tempPos.z = this.startingPos.z + Mathf.Sin(Time.time * speed + this.seed) * horizontalRadius;

        this.transform.position = tempPos;
    }

    void OnDrawGizmos()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(this.transform.position, horizontalRadius + 0.3f);
    }
}
