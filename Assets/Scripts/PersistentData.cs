﻿
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[System.Serializable]
public class PersistentData
{
    public string Name;
    public float BestTime;
    public float Red;
    public float Green;
    public float Blue;
    public float Sound;
    public float Music;
    public float MouseSensitivity;

    public static PersistentData LoadOrDefault()
    {
        PersistentData pd = new PersistentData();
        if (File.Exists(Application.dataPath + "/state.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.dataPath + "/state.dat", FileMode.Open);
            pd = (PersistentData)bf.Deserialize(file);
            file.Close();
        }
        else
        {
            pd.Name = "Player";
            pd.BestTime = 60 * 5;
            pd.Red = 1;
            pd.Green = 1;
            pd.Blue = 1;
            pd.Sound = 0;
            pd.Music = -30;
            pd.MouseSensitivity = 2.0f;
        }

        return pd;
    }

    public static void Save(PersistentData pd)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.dataPath + "/state.dat");
        bf.Serialize(file, pd);
        file.Close();
    }
}
