﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public float Speed = 10;
    public float SpeedInAir = 5;
    public float SpeedToStartTrail = 15;

    public float MouseSensitivityX = 2;
    public float MouseSensitivityY = 2;

    float rotationX = 0;
    float rotationY = 0;

    int jumpCharges = 1;

    public float BurstRechargeTime = 2.5f;
    float LastBurstTime;

    Engine engine;

    LineRenderer lr;

    public Rigidbody RBody;

    // Dead reckoning pozice
    float LastTime;
    Vector3 LastPosition;
    public Vector3 PositionVelocity;
    Vector3 LastPositionVelocity;
    public Vector3 PositionAcceleration;

    //TextMesh tmBurstStatus;

    ParticleSystem psSpeedTrail;
    ParticleSystem psCollision;

    public bool IsControled;

    private AudioSource aSJump;

    RawImage arrow;
    RawImage arrowFull;

    GameObject directionToGlobe;

    void Start ()
    {
        this.RBody = this.transform.GetComponent<Rigidbody>();
        this.engine = GameObject.Find("Engine").GetComponent<Engine>();

        this.LastTime = Time.time;
        this.LastPosition = this.transform.position;
        this.PositionVelocity = Vector3.zero;
        this.LastPositionVelocity = Vector3.zero;
        this.PositionAcceleration = Vector3.zero;

        //this.tmBurstStatus = this.transform.Find("Burst").gameObject.GetComponent<TextMesh>();
        this.psSpeedTrail = this.transform.Find("Effects/SpeedTrail").gameObject.GetComponent<ParticleSystem>();
        this.psCollision = this.transform.Find("Effects/Direction/Collision").gameObject.GetComponent<ParticleSystem>();
        this.arrow = Camera.main.transform.Find("Canvas/Game/Acceleration/Arrow").gameObject.GetComponent<RawImage>();
        this.arrowFull = Camera.main.transform.Find("Canvas/Game/Acceleration/ArrowFull").gameObject.GetComponent<RawImage>();
        this.directionToGlobe = this.transform.Find("DirectionToGlobe").gameObject;


        Camera.main.transform.parent = this.transform;
        Camera.main.transform.localPosition = new Vector3(0, 1, -3);

        this.lr = this.GetComponent<LineRenderer>();

        this.LastBurstTime = 0;

        this.IsControled = true;

        this.aSJump = this.transform.Find("Sounds/Jump").gameObject.GetComponent<AudioSource>();
    }

    void Update()
    {
        bool onGround = false;

        // Zjisteni, zda je na zemi
        RaycastHit[] hits;
        hits = Physics.RaycastAll(transform.position, -transform.up, 1.1F);
        if (hits.Length > 0)
        {
            jumpCharges = 1;
            onGround = true;
        }

        // **************** Skok **********************
        if (this.IsControled && Input.GetButtonDown("Jump"))
        {
            if (jumpCharges > 0)
            {
                Vector3 temp = this.RBody.velocity;
                temp.y = 0;
                this.RBody.velocity = temp;

                float force = hits.Length == 0 ? 6 : 5;

                this.RBody.AddForce(new Vector3(0, force, 0), ForceMode.Impulse);

                this.aSJump.Play();

                --jumpCharges;
            }
        }

        Vector3 cameraV = this.transform.forward;
        cameraV = cameraV.normalized * 20;

        this.lr.SetPosition(0, this.transform.position);
        this.lr.SetPosition(1, this.transform.position + cameraV);

        // **************** Burst **********************
        float actualBurstTime = Time.time - this.LastBurstTime;

        float state = Mathf.Clamp(actualBurstTime / this.BurstRechargeTime, 0 , 1);

        if (state != 1)
        {
            Color tempColor = Color.white;
            tempColor.a = state;
            this.arrow.color = tempColor;
            this.arrowFull.color = Color.clear;
        }
        else
        {
            this.arrow.color = Color.clear;
            this.arrowFull.color = Color.white;
        }

        if (this.IsControled && !onGround && Input.GetMouseButtonDown(0))
        {
            if (actualBurstTime > this.BurstRechargeTime)
            {
                this.RBody.velocity = Vector3.zero;
                this.RBody.AddForce(cameraV, ForceMode.Impulse);
                this.LastBurstTime = Time.time;
            }
        }

        // Kontrola aby nevyletel z mapy 30x30x30
        Vector3 tempPos = this.transform.position;
        if (tempPos.x > 30)
            tempPos.x = 30;
        if (tempPos.y > 30)
            tempPos.y = 30;
        if (tempPos.z > 30)
            tempPos.z = 30;

        if (tempPos.x < 0)
            tempPos.x = 0;
        if (tempPos.y < 0)
            tempPos.y = 0;
        if (tempPos.z < 0)
            tempPos.z = 0;

        this.transform.position = tempPos;

        // Pozice kamery
        Ray ray = new Ray(this.transform.TransformPoint(new Vector3(0, 1, 0)), -Camera.main.transform.forward);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 3))
        {
            Camera.main.transform.localPosition = new Vector3(0, 1, -hit.distance);
            // Do something with the object that was hit by the raycast.
        }
        else
            Camera.main.transform.localPosition = new Vector3(0, 1, -3);


        // Particle pri velke rychlosti
        if(!this.psSpeedTrail.isEmitting)
        {
            if (this.RBody.velocity.magnitude > this.SpeedToStartTrail)
            {
                this.psSpeedTrail.Play();
            }
        }
        else
        {
            if (this.RBody.velocity.magnitude < this.SpeedToStartTrail)
            {
                this.psSpeedTrail.Stop();
            }
        }

        // **************************** Natoceni sipky *********************************************
        GameObject globe = this.engine.GetCurrentGlobe();

        this.directionToGlobe.transform.rotation =
            Quaternion.LookRotation(globe.transform.position - this.transform.position);
    }

    private void FixedUpdate()
    {
        if (this.IsControled)
        {
            // **************** ROTACE **********************
            rotationY += Input.GetAxis("Mouse Y") * this.MouseSensitivityY;
            rotationX += Input.GetAxis("Mouse X") * this.MouseSensitivityX;

            if (rotationY > 90) rotationY = 90;
            if (rotationY < -90) rotationY = -90;

            this.RBody.rotation = Quaternion.Euler(-rotationY, rotationX, 0);

            // **************** Pohyb **********************
            float moveHorizontal = 0;
            float moveVertical = 0;

            //if (input.getkey("a")) movehorizontal += -1;
            //if (input.getkey("d")) movehorizontal += 1;

            //if (input.getkey("s")) movevertical += -1;
            //if (input.getkey("w"))
            //    movevertical += 1;


            moveHorizontal = Input.GetAxis("Horizontal");
            moveVertical = Input.GetAxis("Vertical");
            //Debug.Log("X " + moveHorizontal);

            RaycastHit[] hits;
            hits = Physics.RaycastAll(transform.position, -transform.up, 1.1F);

            if (hits.Length > 0) // Pohyb na zemi
            {
                Vector3 movement = this.RBody.velocity;
                movement.x = moveHorizontal * this.Speed;
                movement.z = moveVertical * this.Speed;
                Vector3 rotatedVector = Quaternion.AngleAxis(this.transform.eulerAngles.y, Vector3.up) * movement;
                this.RBody.velocity = rotatedVector;
            }
            else // pohyb ve vzduchu
            {
                Vector3 movement = this.RBody.velocity;
                // Natoceni na vychozi stav
                Vector3 rotatedVector = Quaternion.AngleAxis(-this.transform.eulerAngles.y, Vector3.up) * movement;

                // Nelze zvysovat nad urcitou rychlost, ale lze zpomalit
                if (rotatedVector.x > 0)
                {
                    if (moveHorizontal > 0)
                    {
                        if (rotatedVector.x + moveHorizontal * this.SpeedInAir < this.SpeedInAir)
                            rotatedVector.x += moveHorizontal * this.SpeedInAir;
                    }
                    else
                    {
                        rotatedVector.x += moveHorizontal * this.SpeedInAir;
                    }
                }
                else
                {
                    if (moveHorizontal < 0)
                    {
                        if (rotatedVector.x + moveHorizontal * this.SpeedInAir > -this.SpeedInAir)
                            rotatedVector.x += moveHorizontal * this.SpeedInAir;
                    }
                    else
                    {
                        rotatedVector.x += moveHorizontal * this.SpeedInAir;
                    }
                }

                // Nelze zvysovat nad urcitou rychlost, ale lze zpomalit
                if (rotatedVector.z > 0)
                {
                    if (moveVertical > 0)
                    {
                        if (rotatedVector.z + moveVertical * this.SpeedInAir < this.SpeedInAir)
                            rotatedVector.z += moveVertical * this.SpeedInAir;
                    }
                    else
                    {
                        rotatedVector.z += moveVertical * this.SpeedInAir;
                    }
                }
                else
                {
                    if (moveVertical < 0)
                    {
                        if (rotatedVector.z + moveVertical * this.SpeedInAir > -this.SpeedInAir)
                            rotatedVector.z += moveVertical * this.SpeedInAir;
                    }
                    else
                    {
                        rotatedVector.z += moveVertical * this.SpeedInAir;
                    }
                }

                // Natoceni podle uhlu pohledu
                rotatedVector = Quaternion.AngleAxis(this.transform.eulerAngles.y, Vector3.up) * rotatedVector;
                this.RBody.velocity = rotatedVector;
            }
        }

        // Deadreckoning pozice
        float dTime = Time.time - this.LastTime;
        // Vypocet
        this.PositionVelocity = (this.transform.position - this.LastPosition) / dTime;
        this.PositionAcceleration = (this.PositionVelocity - this.LastPositionVelocity) / dTime;
        // Uchovani posledniho stavu
        this.LastTime = Time.time;
        this.LastPosition = this.transform.position;
        this.LastPositionVelocity = this.PositionVelocity;
    }

    public void SetMouseSensitivity(float value)
    {
        this.MouseSensitivityX = value;
        this.MouseSensitivityY = value;
    }

    public void PlayCollisionEffect(Vector3 otherPlayer)
    {
        this.psCollision.transform.parent.transform.rotation =
            Quaternion.LookRotation(otherPlayer - this.transform.position);

        this.psCollision.Play();
    }

    public bool IsAccelerationOn()
    {
        return this.psSpeedTrail.isEmitting;
    }
}
