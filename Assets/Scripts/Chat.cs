﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Chat : MonoBehaviour
{
    public bool Active;
    public int MaxLines = 5;
    public int fontSize = 10;
    Engine engine;
    GameObject goChat;
    GameObject goInput;
    TMP_InputField input;
    TextMeshProUGUI label;

    Dictionary<Guid, Tuple<string, string>> lastTweets;
    float lastTweetTime;
    float tweetDeleteDelay = 10;

    void Start ()
    {
        this.Active = false;
        this.engine = GameObject.Find("Engine").GetComponent<Engine>();
        this.goChat = Camera.main.transform.Find("Canvas/Chat").gameObject;
        this.goInput = Camera.main.transform.Find("Canvas/Chat/Tweet").gameObject;
        this.input = Camera.main.transform.Find("Canvas/Chat/Tweet/Input").gameObject.GetComponent<TMP_InputField>();
        this.label = Camera.main.transform.Find("Canvas/Chat/Tweet/Label").gameObject.GetComponent<TextMeshProUGUI>();

        this.lastTweets = new Dictionary<Guid, Tuple<string, string>>();

        this.goInput.SetActive(false);
        lastTweetTime = 0;
    }

    private void FixedUpdate()
    {
        // Mazani chatu po chvili
        if(Time.time - this.lastTweetTime > this.tweetDeleteDelay)
        {
            this.DestroyFirstLine();
            this.lastTweetTime = Time.time;
        }
    }

    public void RemovePlayer(Guid id)
    {
        this.lastTweets.Remove(id);
    }

    public void UpdateTweet(Guid id, Color lineColor, string name, string tweet)
    {
        if (this.lastTweets.ContainsKey(id))
        {
            if(this.lastTweets[id].Item2 != tweet)
            {
                this.lastTweets[id] = new Tuple<string, string>(name, tweet);
                string t = name + ": " + tweet;         
                this.AddNewLine(lineColor, t);
                this.lastTweetTime = Time.time;
            }
        }
        else
        {
            this.lastTweets.Add(id, new Tuple<string, string>(name, tweet));
        }
    }

    public void Show(Color color)
    {
        this.goInput.SetActive(true);
        this.label.color = color;
        this.input.ActivateInputField();
        this.Active = true;
    }

    public void Hide()
    {
        this.goInput.SetActive(false);
        this.Active = false;
    }

    public void OnSend()
    {
        if(input.text != "")
            this.engine.SetTweet(input.text);
    }

    private void AddNewLine(Color lineColor, string tweet)
    {
        UnityEngine.Object prefab = Resources.Load("ChatLine");
        GameObject instance = Instantiate(prefab) as GameObject;

        instance.GetComponent<TextMeshProUGUI>().text = tweet;
        instance.GetComponent<TextMeshProUGUI>().color = lineColor;
        instance.transform.parent = this.transform;

        int count = this.goChat.transform.childCount - 1;
        instance.transform.localPosition = new Vector3(0, -count * fontSize, 0);
        instance.transform.localScale = Vector3.one;

        if(count > this.MaxLines)
        {
            this.DestroyFirstLine();
        }
    }

    private void DestroyFirstLine()
    {
        int count = this.goChat.transform.childCount - 1;

        if (count > 0)
        {
            Destroy(this.goChat.transform.GetChild(1).gameObject);

            for (int i = 1; i < count + 1; ++i)
            {
                this.goChat.transform.GetChild(i).localPosition = new Vector3(0, -(i - 1) * fontSize, 0);
            }
        }
    }

    private string GetString(string source1, string source2)
    {
        string test = "šč0";
        byte[] test1 = System.Text.Encoding.UTF8.GetBytes(test);



        byte[] temp1 = System.Text.Encoding.UTF8.GetBytes(source1);
        string res = System.Text.Encoding.UTF8.GetString(temp1);
        byte[] res1 = System.Text.Encoding.UTF8.GetBytes(res);



        byte[] temp2 = System.Text.Encoding.UTF8.GetBytes(source2);

        byte[] temp = new byte[temp1.Length + temp2.Length];

        System.Buffer.BlockCopy(temp1, 0, temp, 0, temp1.Length);
        System.Buffer.BlockCopy(temp2, 0, temp, temp1.Length, temp2.Length);

        return System.Text.Encoding.UTF8.GetString(temp);
    }
}
