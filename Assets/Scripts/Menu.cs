﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    Engine engine;

    string startingName;
    float startingRed;
    float startingGreen;
    float startingBlue;

    float startingSound;
    float startingMusic;
    float startingMouseSensitivity;

    TMP_InputField playersName;

    Slider sliderRed;
    Slider sliderGreen;
    Slider sliderBlue;

    Slider sliderSound;
    Slider sliderMusic;
    Slider sliderMouseSensitivity;

    void Start()
    {
        this.engine = GameObject.Find("Engine").GetComponent<Engine>();

        this.playersName = Camera.main.transform.Find("Canvas/Menu/Name").gameObject.GetComponent<TMP_InputField>();
        this.playersName.text = this.startingName;

        this.sliderRed = Camera.main.transform.Find("Canvas/Menu/SliderRed").gameObject.GetComponent<Slider>();
        this.sliderGreen = Camera.main.transform.Find("Canvas/Menu/SliderGreen").gameObject.GetComponent<Slider>();
        this.sliderBlue = Camera.main.transform.Find("Canvas/Menu/SliderBlue").gameObject.GetComponent<Slider>();

        this.sliderSound = Camera.main.transform.Find("Canvas/Menu/SliderSound").gameObject.GetComponent<Slider>();
        this.sliderMusic = Camera.main.transform.Find("Canvas/Menu/SliderMusic").gameObject.GetComponent<Slider>();
        this.sliderMouseSensitivity = Camera.main.transform.Find("Canvas/Menu/SliderSensitivity").gameObject.GetComponent<Slider>();

        this.sliderRed.value = this.startingRed;
        this.sliderGreen.value = this.startingGreen;
        this.sliderBlue.value = this.startingBlue;

        this.sliderSound.value = this.startingSound;
        this.sliderMusic.value = this.startingMusic;
        this.sliderMouseSensitivity.value = this.startingMouseSensitivity;

        this.transform.gameObject.SetActive(false);
    }

    #region Obsluha GUI udalosti

    public void OnEndEditPlayersName()
    {
        this.engine.SetPlayersName(this.playersName.text);
    }

    public void OnColorChange()
    {
        this.engine.SetPlayerColor(this.sliderRed.value, this.sliderGreen.value, this.sliderBlue.value);
    }

    public void OnSoundChange()
    {
        this.engine.SetSound(this.sliderSound.value);
    }

    public void OnMusicChange()
    {
        this.engine.SetMusic(this.sliderMusic.value);
    }

    public void OnSensitivityChange()
    {
        this.engine.SetSensitivity(this.sliderMouseSensitivity.value);
    }

    public void OnExitClick()
    {
        Application.Quit();
    }

    #endregion

    /// <summary>
    /// Volat jen na zacatku pred tim, nez ze vola Start()
    /// </summary>
    /// <param name="name"></param>
    public void SetNameOnStart(string name)
    {
        this.startingName = name;
    }

    public void SetColorOnStart(float r, float g, float b)
    {
        this.startingRed = r;
        this.startingGreen = g;
        this.startingBlue = b;
    }

    public void SetSound(float value)
    {
        this.startingSound = value;
    }
    public void SetMusic(float value)
    {
        this.startingMusic = value;
    }
    public void SetMouseSensitivity(float value)
    {
        this.startingMouseSensitivity = value;
    }
}
