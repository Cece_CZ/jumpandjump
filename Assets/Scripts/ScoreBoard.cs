﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreBoard : MonoBehaviour
{
    public class ScoreLine
    {
        public GameObject GOLine;
        public string Name;
        public float BestTime;
        public int globeIndex;
        public int globeCount;
        public bool IsLocalPlayer;

        public ScoreLine(string name, float bt, int gi, int gc, bool il)
        {
            this.GOLine = null;
            this.Name = name;
            this.BestTime = bt;
            this.globeIndex = gi;
            this.globeCount = gc;
            this.IsLocalPlayer = il;
        }

        public ScoreLine(GameObject line, string name, float bt, int gi, int gc, bool il)
        {
            this.GOLine = line;
            this.Name = name;
            this.BestTime = bt;
            this.globeIndex = gi;
            this.globeCount = gc;
            this.IsLocalPlayer = il;
        }
    }

    GameObject board;

    List<ScoreLine> currentScoreBoard;

	// Use this for initialization
	void Start ()
    {
        this.board = Camera.main.transform.Find("Canvas/ScoreBoard").gameObject;

        this.currentScoreBoard = new List<ScoreLine>();

        this.transform.gameObject.SetActive(false);
    }

    public void UpdateData(PacketPlayerState localPlayer, ConcurrentDictionary<Guid, PacketPlayerState> otherPlayers, int globeCount)
    {
        // Vytvoreni tabulky
        List<ScoreLine> temp = new List<ScoreLine>();

        temp.Add(new ScoreLine(localPlayer.Name, localPlayer.BestTime, localPlayer.GlobeIndex, globeCount, true));

        foreach(var pps in otherPlayers)
        {
            temp.Add(new ScoreLine(pps.Value.Name, pps.Value.BestTime, pps.Value.GlobeIndex, globeCount, false));
        }

        temp.Sort((a, b) => (a.BestTime.CompareTo(b.BestTime)));

        int count = temp.Count;

        // Kontrola poctu - pridavani radku
        while (this.currentScoreBoard.Count < count)
        {
            UnityEngine.Object prefab = Resources.Load("ScoreLine");
            GameObject instance = Instantiate(prefab) as GameObject;
            instance.transform.parent = this.board.transform;

            this.currentScoreBoard.Add(new ScoreLine(instance, "", float.PositiveInfinity, 0, globeCount, false));
        }

        // Kontrola poctu - ubirani
        while (this.currentScoreBoard.Count > count)
        {
            // Zniceni objektu
            Destroy(this.currentScoreBoard[0].GOLine);
            // Odstraneni ze seznamu
            this.currentScoreBoard.RemoveAt(0);       
        }

        // Nastaveni odsazeni a hodnot     
        for(int i = 0; i < count; ++i)
        {
            Vector3 pos = this.currentScoreBoard[i].GOLine.transform.localPosition;
            pos.x = 0;
            pos.y = (i + 1) * -25;
            pos.z = 0;
            this.currentScoreBoard[i].GOLine.transform.localPosition = pos;
            this.currentScoreBoard[i].GOLine.transform.localScale = Vector3.one;

            TextMeshProUGUI tmpName = this.currentScoreBoard[i].GOLine.transform.Find("PlayersName").GetComponent<TextMeshProUGUI>();
            if (this.currentScoreBoard[i].IsLocalPlayer = temp[i].IsLocalPlayer)
                tmpName.color = Color.yellow;
            else
                tmpName.color = Color.white;
            tmpName.text = this.currentScoreBoard[i].Name = temp[i].Name;


            TextMeshProUGUI tmpTime = this.currentScoreBoard[i].GOLine.transform.Find("BestTime").GetComponent<TextMeshProUGUI>();
            if (this.currentScoreBoard[i].IsLocalPlayer)
                tmpTime.color = Color.yellow;
            else
                tmpTime.color = Color.white;
            tmpTime.text = Engine.GetTimeString(this.currentScoreBoard[i].BestTime = temp[i].BestTime);

            TextMeshProUGUI tmpProgress = this.currentScoreBoard[i].GOLine.transform.Find("Progress").GetComponent<TextMeshProUGUI>();
            if (this.currentScoreBoard[i].IsLocalPlayer)
                tmpProgress.color = Color.yellow;
            else
                tmpProgress.color = Color.white;
            tmpProgress.text = (this.currentScoreBoard[i].globeIndex = temp[i].globeIndex) + "/" + (this.currentScoreBoard[i].globeCount = temp[i].globeCount);
        }
    }
}
