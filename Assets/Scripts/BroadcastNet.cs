﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Threading;
using System.Text;
using System.Collections.Concurrent;
using UnityEngine;

/// <summary>
/// Sitove rozhrani zalozene na broadcastu
/// </summary>
public class BroadcastNet
{
    /// <summary>
    /// Port na kterem hra pobezi
    /// </summary>
    int gamePort;

    /// <summary>
    /// Seznam vsech pouzitelnych sitovych rozhrani
    /// </summary>
    List<NetworkInterface> interfaces;

    /// <summary>
    /// Seznam adres k odesilani
    /// </summary>
    List<IPEndPoint> addressForSend;

    /// <summary>
    /// Seznam socketu k odesilani
    /// </summary>
    List<Socket> socketsForSend;

    /// <summary>
    /// Adresa pro prijem packetu
    /// </summary>
    IPEndPoint addressForReceiving;

    /// <summary>
    /// Klient pro odesilani
    /// </summary>
    UdpClient clientForReceiving;

    /// <summary>
    /// Fronta packetu ke zpracovani
    /// </summary>
    public ConcurrentQueue<Byte[]> PacketsQueue;

    /// <summary>
    /// Konstruktor
    /// </summary>
    /// <param name="port">Port na kterem hra pobezi</param>
    public BroadcastNet(int port)
    {
        Debug.Log("*** Start");
        this.gamePort = port;

        this.PacketsQueue = new ConcurrentQueue<Byte[]>();

        // Hledani siti
        this.interfaces = new List<NetworkInterface>();
        NetworkInterface[] allInterfaces = NetworkInterface.GetAllNetworkInterfaces();
        Debug.Log("*** System interfaces count: " + allInterfaces.Length);
        foreach (NetworkInterface ni in allInterfaces)
        {
            if (ni.OperationalStatus == OperationalStatus.Up)
            {
                IPInterfaceProperties IP4P = ni.GetIPProperties();

                int count = IP4P.UnicastAddresses.Count;
                for (int i = 0; i < count; ++i)
                {
                    Debug.Log("*** UnicastAddresses " + IP4P.UnicastAddresses[i].Address);
                    if (IP4P.UnicastAddresses[i].Address.ToString().Contains("."))
                    {
                        interfaces.Add(ni);
                        break;
                    }
                }
            }
        }

        // Posilani packetu
        this.addressForSend = new List<IPEndPoint>();
        this.socketsForSend = new List<Socket>();
        Debug.Log("*** Usable interfaces count: " + interfaces.Count);
        foreach (NetworkInterface ni in interfaces)
        {
            // Vytvareni socketu
            Socket sending_socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPAddress send_to_address = GetBroadcastIP(ni);
            Debug.Log("*** BroadcastIP " + send_to_address);
            IPEndPoint sending_end_point = new IPEndPoint(send_to_address, gamePort); 

            this.addressForSend.Add(sending_end_point);
            this.socketsForSend.Add(sending_socket);
        }

        // Prijem Packetu
        this.addressForReceiving = new IPEndPoint(IPAddress.Any, gamePort);
        this.clientForReceiving = new UdpClient();
        this.clientForReceiving.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true); // Musi se nastavit, jinak na jednom PC nefunguje vic instacni
        this.clientForReceiving.Client.Bind(this.addressForReceiving);
        this.clientForReceiving.BeginReceive(new AsyncCallback(ReceiveCallback), null);
    }

    /// <summary>
    /// Zakladni funkce prijimajici packety
    /// </summary>
    /// <param name="ar"></param>
    public void ReceiveCallback(IAsyncResult ar)
    {
        // Odebereme prijate data
        Byte[] receiveBytes = clientForReceiving.EndReceive(ar, ref this.addressForReceiving);

        // Hodine do fronty
        this.PacketsQueue.Enqueue(receiveBytes);

        // Cekame na dalsi
        this.clientForReceiving.BeginReceive(new AsyncCallback(ReceiveCallback), null);
    }

    /// <summary>
    /// Odesilani packetu
    /// </summary>
    /// <param name="dataToSend"></param>
    public void SendPacket(byte[] dataToSend)
    {
        int count = this.addressForSend.Count;

        for(int i = 0; i < count; ++i)
        {
            try
            {
                this.socketsForSend[i].SendTo(dataToSend, this.addressForSend[i]);
            }
            catch (Exception ex)
            {
                //System.Console.WriteLine("Fail to send data to " + this.interfaces[i].Name + " with : " + ex.Message);
                Debug.Log("Fail to send data to " + this.interfaces[i].Name + " with : " + ex.Message);
            }
        }
    }

    /// <summary>
    /// Ziskani broadcastu pro sitove rozhrani, pocita s tim, ze uz je validni
    /// </summary>
    /// <param name="ni"></param>
    /// <returns></returns>
    public static IPAddress GetBroadcastIP(NetworkInterface ni)
    {
        byte[] broadcastIPBytes = new byte[4];

        IPInterfaceProperties IP4P = ni.GetIPProperties();
        int count = IP4P.UnicastAddresses.Count;
        for (int j = 0; j < count; ++j)
        {
            if (IP4P.UnicastAddresses[j].Address.ToString().Contains(".")) // Musi se kontrolovat, poradi informaci neni zaruceno
            {
                IPAddress maskIP = ni.GetIPProperties().UnicastAddresses[j].IPv4Mask;
                IPAddress hostIP = ni.GetIPProperties().UnicastAddresses[j].Address;
                Debug.Log("*** Mask " + maskIP);

                byte[] complementedMaskBytes = new byte[4];

                for (int i = 0; i < 4; i++)
                {
                    complementedMaskBytes[i] = (byte)~(maskIP.GetAddressBytes()[i]); // Logicka negace
                    broadcastIPBytes[i] = (byte)((hostIP.GetAddressBytes()[i]) | complementedMaskBytes[i]); // Logicky soucet s maskou
                }
            }
        }
        return new IPAddress(broadcastIPBytes);
    }
}
