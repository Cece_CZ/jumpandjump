﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerExternal : MonoBehaviour
{
    string playersName;
    float Red;
    float Green;
    float Blue;

    public float SpeedToStartTrail = 15;

    TextMesh textName;

    ParticleSystem psSpeedTrail = null;
    ParticleSystem psCollision = null;

    Vector3 lastPosition;

    float minEffectLasting = 0.5f; 
    float fromLastStop = 0;

    // Use this for initialization
    void Start ()
    {
        this.textName = this.transform.Find("Name").gameObject.GetComponent<TextMesh>();

        this.psSpeedTrail = this.transform.Find("Effects/SpeedTrail").gameObject.GetComponent<ParticleSystem>();
        this.psCollision = this.transform.Find("Effects/Direction/Collision").gameObject.GetComponent<ParticleSystem>();

        this.lastPosition = this.transform.position;
    }

    //void Update()
    //{
    //    Vector3 temp = this.transform.position - this.lastPosition;
    //    this.lastPosition = this.transform.position;

    //    // Particle pri velke rychlosti
    //    if (!this.psSpeedTrail.isEmitting)
    //    {
            
    //        if (temp.magnitude > SpeedToStartTrail * Time.deltaTime)
    //        {
    //            Debug.Log(temp.magnitude + " > " + (SpeedToStartTrail * Time.deltaTime));
    //            Debug.Log("Start " + Time.time);
    //            this.psSpeedTrail.Play();
    //            this.fromLastStop = 0;
    //        }
    //    }
    //    else
    //    {            
    //        if (temp.magnitude < SpeedToStartTrail * Time.deltaTime)
    //        {
    //            Debug.Log(temp.magnitude + " < " + (SpeedToStartTrail * Time.deltaTime));
    //            this.fromLastStop += Time.deltaTime;
    //            if (this.fromLastStop > this.minEffectLasting)
    //            {
    //                this.psSpeedTrail.Stop();
    //            }
    //        }
    //        else
    //        {
    //            this.fromLastStop = 0; // Uchovava pousteni castic
    //        }
    //    }
    //}

    public void SetName(string name)
    {
        this.textName.transform.rotation = Camera.main.transform.rotation;
        if (this.playersName != name)
        {
            this.textName.text = name;
            this.playersName = name;
        }
    }

    public void SetColor(float r, float g, float b)
    {
        if(this.Red != r || this.Green != g || this.Blue != b)
        {
            this.transform.Find("Sphere").GetComponent<MeshRenderer>().material.color = new Color(r, g, b);
            this.Red = r;
            this.Green = g;
            this.Blue = b;
        }
    }

    public void PlayCollisionEffect(Vector3 otherPlayer)
    {
        this.psCollision.transform.parent.transform.rotation =
            Quaternion.LookRotation(otherPlayer - this.transform.position);

        this.psCollision.Play();
    }

    public void StartEffect()
    {
        if(this.psSpeedTrail != null)
            if(!this.psSpeedTrail.isEmitting)
                this.psSpeedTrail.Play();
    }

    public void StopEffect()
    {
        if (this.psSpeedTrail != null)
            if (this.psSpeedTrail.isEmitting)
                this.psSpeedTrail.Stop();
    }

}
