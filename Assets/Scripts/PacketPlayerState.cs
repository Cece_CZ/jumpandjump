﻿using System;
using System.IO;
using System.Text;

public class PacketPlayerState
{
    // Identifikator packetu
    // TODO: Misto pro optimalizaci serializaci polem
    public byte PacketID0 = 74; // "J"
    public byte PacketID1 = 97; // "a"
    public byte PacketID2 = 83; // "S"

    public Guid ID;

    public string Name;
    public float BestTime;

    public float Red;
    public float Green;
    public float Blue;

    // TODO: Misto pro optimalizaci serializaci polem
    public float PositionX;
    public float PositionY;
    public float PositionZ;

    public float GlobePositionX;
    public float GlobePositionY;
    public float GlobePositionZ;

    //public float PositionVelocityX;
    //public float PositionVelocityY;
    //public float PositionVelocityZ;

    //public float PositionAccelerationX;
    //public float PositionAccelerationY;
    //public float PositionAccelerationZ;

    public int Acceleration;
    public int GlobeIndex;
    public string Tweet;

    // Data, co se neserializuji
    public DateTime ReceivingTime;

    public PacketPlayerState()
    {
        this.ID = Guid.Empty;

        this.Name = "";
        this.BestTime = float.PositiveInfinity;

        this.Red = 1;
        this.Green = 1;
        this.Blue = 1;

        this.PositionX = 0;
        this.PositionY = 0;
        this.PositionZ = 0;

        this.GlobePositionX = 0;
        this.GlobePositionY = 0;
        this.GlobePositionZ = 0;

        //this.PositionVelocityX = 0;
        //this.PositionVelocityY = 0;
        //this.PositionVelocityZ = 0;

        //this.PositionAccelerationX = 0;
        //this.PositionAccelerationY = 0;
        //this.PositionAccelerationZ = 0;

        this.Acceleration = 0;
        this.GlobeIndex = 0;
        this.Tweet = "";
    }

    public byte[] GetInBytes()
    {
        byte[] binaryData;
        using (MemoryStream ms = new MemoryStream())
        {
            using (BinaryWriter bw = new BinaryWriter(ms))
            {
                bw.Write(this.PacketID0);
                bw.Write(this.PacketID1);
                bw.Write(this.PacketID2);

                bw.Write(this.ID.ToByteArray());
                bw.Write(this.Name);
                bw.Write(this.BestTime);

                bw.Write(Red);
                bw.Write(Green);
                bw.Write(Blue);

                bw.Write(PositionX);
                bw.Write(PositionY);
                bw.Write(PositionZ);

                bw.Write(GlobePositionX);
                bw.Write(GlobePositionY);
                bw.Write(GlobePositionZ);

                //bw.Write(PositionVelocityX);
                //bw.Write(PositionVelocityY);
                //bw.Write(PositionVelocityZ);

                //bw.Write(PositionAccelerationX);
                //bw.Write(PositionAccelerationY);
                //bw.Write(PositionAccelerationZ);

                bw.Write(Acceleration);
                bw.Write(GlobeIndex);

                bw.Write(this.Tweet);

                binaryData = ms.ToArray();
            }
        }
        return binaryData;
    }

    public long SetSerialized(byte[] bytes)
    {
        long readerPosition = 0;
        using (MemoryStream ms = new MemoryStream(bytes))
        {
            using (BinaryReader br = new BinaryReader(ms))
            {
                br.ReadBytes(3);

                this.ID = new Guid(br.ReadBytes(16));

                this.Name = br.ReadString();
                this.BestTime = br.ReadSingle();

                this.Red = br.ReadSingle();
                this.Green = br.ReadSingle();
                this.Blue = br.ReadSingle();

                this.PositionX = br.ReadSingle();
                this.PositionY = br.ReadSingle();
                this.PositionZ = br.ReadSingle();

                this.GlobePositionX = br.ReadSingle();
                this.GlobePositionY = br.ReadSingle();
                this.GlobePositionZ = br.ReadSingle();

                //this.PositionVelocityX = br.ReadSingle();
                //this.PositionVelocityY = br.ReadSingle();
                //this.PositionVelocityZ = br.ReadSingle();

                //this.PositionAccelerationX = br.ReadSingle();
                //this.PositionAccelerationY = br.ReadSingle();
                //this.PositionAccelerationZ = br.ReadSingle();

                this.Acceleration = br.ReadInt32();
                this.GlobeIndex = br.ReadInt32();
                this.Tweet = br.ReadString();

                readerPosition = br.BaseStream.Position;
            }
        }
        return readerPosition;
    }
}
